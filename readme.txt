=== Plugin Name ===
Contributors: bjorsq
Donate link: http://code.google.com/p/event-post-type/
Tags: events, custom post type, calendar
Requires at least: 3.1
Tested up to: 3.5alpha
Stable tag: 1.1
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin creates a new post type for Events

== Description ==

This plugin creates a new post type for Events, and includes the following features:

* a shortcode to display events as a list or calendar on any page
* a widget to display events as a list or calendar in a sidebar
* feed support for JSON and iCal calendar formats
* customisable templates for events archives and events pages
* options for different date formats, number of events per page, etc.
* option to make events "sticky"
* i18n support (translators needed)

== Installation ==

Install through the Wordpress dashboard, or download, unpack and FTP to your wordpress plugins folder.
Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Can I contribute to the development of this plugin =

Oh go on then I suppose so. Head over to the plugin's git repository on bitbucket and fork it. I do the development using git, then shove the code over to the Wordpress SVN repo when I have something stable enough to tag.

I've recently made the plugin i18n-aware, so any translations would be most welcome

= What about foo bar? =

Ah yes, foo bar, deary me, what a dilemma.
Please ask questions in the forum - I'll post any frequent ones here

== Changelog ==

= 1.2 =
* Made the plugin i18n aware (I need translators!)
* Bundled taxonomies with the plugin (previously used core `categories` taxonomy)
* Bundled templates with the plugin

= 1.1 =
* First major release - used and tested for about a year on some of the Wordpress installations in Leeds University.

= 1.0 =
* First version (proof of concept really)

== Upgrade Notice ==

= 1.2 =
This release attempts to make events display easier - the major change is the bundling of taxonomies with the plugin (any events in wordpres categories should get new events categories made for them in the upgrade process).

= 1.1 =
Although this release was pretty stable, the display of Events was very badly handled.

= 1.0 =
This version was so basic it probably still works, but is bound to break horribly when Wordpress changes its API again